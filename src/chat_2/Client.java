/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class Client {

	// for I/O
	private ObjectInputStream sInput;		// to read from the socket
	private ObjectOutputStream sOutput;		// to write on the socket
	private Socket socket;
	private ArrayList<String> serverList;
	static InetAddress ipdestino;

	// if I use a GUI or not
	private ClientGUI cg;

	// the server, the port and the username
	private String server, username;
	private int port;

	/*
	 * Constructor call when used from a GUI
	 * in console mode the ClienGUI parameter is null
	 */
	Client(String server, int port, String username, ClientGUI cg) {
		this.server = server;
		this.port = port;
		this.username = username;
		// save if we are in GUI mode or not
		this.cg = cg;
		ServerFileReader(this.port);
	}

	Client() {
	}

	/*
	 * To start the dialog
	 */
	public boolean start() {
		// try to connect to the server
		try {
			socket = new Socket(server, port);
		} // if it failed not much I can so
		catch (Exception ec) {
			display("Error connectiong to server:" + ec);
			return false;
		}

		String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.
			getPort();
		display(msg);

		/* Creating both Data Stream */
		try {
			sInput = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException eIO) {
			display("Exception creating new Input/output Streams: " + eIO);
			return false;
		}

		// creates the Thread to listen from the server
		new ListenFromServer().start();
		// Send our username to the server this is the only message that we
		// will send as a String. All other messages will be ChatMessage objects
		try {
			sOutput.writeObject(username);

		} catch (IOException eIO) {
			display("Exception doing login : " + eIO);
			disconnect();
			return false;
		}
		// success we inform the caller that it worked
		return true;
	}

	/*
	 * To send a message to the console or the GUI
	 */
	private void display(String msg) {
		cg.append(msg + "\n");		// append to the ClientGUI JTextArea (or whatever)
	}

	/*
	 * To send a message to the server
	 */
	void sendMessage(ChatMessage msg) {
		try {
			sOutput.writeObject(msg);
		} catch (IOException e) {
			display("Exception writing to server: " + e);
		}
	}

	/*
	 * When something goes wrong
	 * Close the Input/Output streams and disconnect not much to do in the catch clause
	 */
	private void disconnect() {
		try {
			if (sInput != null) {
				sInput.close();
			}
		} catch (Exception e) {
		} // not much else I can do
		try {
			if (sOutput != null) {
				sOutput.close();
			}
		} catch (Exception e) {
		} // not much else I can do
		try {
			if (socket != null) {
				socket.close();
			}
		} catch (Exception e) {
		} // not much else I can do

		// inform the GUI
		if (cg != null) {
			cg.connectionFailed();
		}

	}

	/*
	 * a class that waits for the message from the server and append them to the JTextArea
	 * if we have a GUI or simply System.out.println() it in console mode
	 */
	class ListenFromServer extends Thread {

		@Override
		public void run() {
			while (true) {
				try {
					String msg = (String) sInput.readObject();
					if (msg.equalsIgnoreCase("Username em uso\n")) { // verificação da mensagem enviado pelo servidor de o username já estiver em uso
						disconnect();
					}
					cg.append(msg);
				} catch (IOException e) {
					display("Server has close the connection: " + e);
					if (cg != null) {
						cg.connectionFailed();
					}
					break;
				} // can't happen with a String object but need the catch anyhow
				catch (ClassNotFoundException e2) {
				}
			}
		}
	}

	public ArrayList ServerFileReader(int port) {

		serverList = new ArrayList<>();

		try {

			//Create object of FileReader
			FileReader inputFile = new FileReader("servers.txt");

			//Instantiate the BufferedReader Class
			BufferedReader bufferReader = new BufferedReader(inputFile);

			//Variable to hold the one line data
			String line;

			// Read file line by line and print on the console
			while ((line = bufferReader.readLine()) != null) {
				if (hostAvailabilityCheck(line, port)) {
					serverList.add(line);
				}
			}

			//Close the buffer reader
			bufferReader.close();
		} catch (Exception e) {
			//#####//
		}

		if (serverList.isEmpty()) {
			System.out.println("nao ha servidores");
		} else {
			System.out.println("Lista de Servidores:");
			for (String se : serverList) {
				System.out.println(se);
			}
		}
		return serverList;
	}

	public static boolean hostAvailabilityCheck(String serv, int port) throws InterruptedException, IOException {
		try {
			InetAddress host = InetAddress.getByName(serv);
			DatagramSocket socket = new DatagramSocket(port, host);
			DatagramPacket packet = new DatagramPacket(new byte[100], 0, host, port);
			socket.send(packet);
			socket.receive(packet);
			socket.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
